..
   RestructuredText: warning and note directives are not properly rendered #1682
   https://github.com/github/markup/issues/1682

.. note::
   This repository has been moved. Please check the new locations below.

   * `~grauwoelfchen/tdd-bear`_ on Sourcehut
   * `grauwoelfchen/tdd-bear`_ on Codeberg

.. _`~grauwoelfchen/tdd-bear`: https://git.sr.ht/~grauwoelfchen/tdd-bear
.. _`grauwoelfchen/tdd-bear`: https://codeberg.org/grauwoelfchen/tdd-bear


TDD-Bear
========

``Test-Driven Development Samples``

* Rust
* Go
* C#
* Python


To-Do
-----

* Move CI jobs to the readonly Woodpecker CI instance on my home server
* Publish builder images to the package registry on Codeberg.org
* Remove unnecessary branches isolation


Packages
--------

.. |rust-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/rust/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/rust

.. |rust-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/rust/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/rust

.. |rust-xunit-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/rust-xunit/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/rust-xunit

.. |rust-xunit-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/rust-xunit/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/rust-xunit

.. |go-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/go/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/go

.. |go-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/go/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/go

.. |go-xunit-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/go-xunit/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/go-xunit

.. |go-xunit-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/go-xunit/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/go-xunit

.. |csharp-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/csharp/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/csharp

.. |csharp-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/csharp/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/csharp

.. |csharp-xunit-pipeline| image:: https://gitlab.com/grauwoelfchen/tdds/badges/csharp-xunit/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/csharp-xunit

.. |csharp-xunit-coverage| image:: https://gitlab.com/grauwoelfchen/tdds/badges/csharp-xunit/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/tdds/commits/csharp-xunit


+--------------+-------------------------+-------------------------+
| Package      | Pipeline                | Coverage                |
+==============+=========================+=========================+
| rust         | |rust-pipeline|         | |rust-coverage|         |
+--------------+-------------------------+-------------------------+
| rust-xunit   | |rust-xunit-pipeline|   | |rust-xunit-coverage|   |
+--------------+-------------------------+-------------------------+
| go           | |go-pipeline|           | |go-coverage|           |
+--------------+-------------------------+-------------------------+
| go-xunit     | |go-xunit-pipeline|     | |go-xunit-coverage|     |
+--------------+-------------------------+-------------------------+
| csharp       | |csharp-pipeline|       | |csharp-coverage|       |
+--------------+-------------------------+-------------------------+
| csharp-xunit | |csharp-xunit-pipeline| | |csharp-xunit-coverage| |
+--------------+-------------------------+-------------------------+
| python       |                         |                         |
+--------------+-------------------------+-------------------------+


Inspired Books
--------------

* Test-Driven Development By Examples (9780321146533)
* Learning Test-Driven Development (9781098106478)


License
-------

``GPL-3.0-or-later``
